import React, {useState} from 'react';
import "./App.css";
import Header from './components/Header';

function App() {
  const [showDate, setShowDate] = useState(false);
  const today = new Date().toLocaleString();
  
  return (
    <>
      <Header>
        <h1>Fatec 2022</h1>
      </Header>
      <div className="container">
        <button type="button" onClick={() => setShowDate(!showDate)}>Show Date</button>
        {showDate && <p>{today}</p>}
      </div>
    </>
    
  );
}

export default App;
