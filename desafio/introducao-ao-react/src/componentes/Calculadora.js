import React from 'react';
import Valor from './Valor';
import Memoria from './Memoria'
import Digitos from './Digitos'

export default function Calculadora() {
    return(
        <div className='Calculadora'>
            <div className='titulo'>
                <span>Calculadora</span>
            </div>
            <Valor/>
            <Memoria/>
            <Digitos/>
        </div>
    );
}