import React from 'react';

export default function Memoria() {
    return(
        <div className="digitos">
            <table>
                <tr>
                    <td><button onclick="Operacao('^')" className="Ope">xⁿ</button></td>
                    <td><button onclick="Apagar('v')" className="Ope">CE</button></td>
                    <td><button onclick="Apagar('t')" className="Ope">C</button></td>
                    <td><button onclick="Apagar('u')" className="Ope">←</button></td>
                </tr>
                <tr>
                    <td><button onclick="ColocarParenteses('(')" className="Ope">(</button></td>
                    <td><button onclick="ColocarParenteses(')')" className="Ope">)</button></td>
                    <td><button onclick="Operacao('√')" className="Ope">√</button></td>
                    <td><button onclick="Operacao('/')" className="Ope">÷</button></td>
                </tr>
                <tr>
                    <td><button onclick="AdcionarChar('7')" className="Num">7</button></td>
                    <td><button onclick="AdcionarChar('8')" className="Num">8</button></td>
                    <td><button onclick="AdcionarChar('9')" className="Num">9</button></td>
                    <td><button onclick="Operacao('*')" className="Ope">X</button></td>
                </tr>
                <tr>
                    <td><button onclick="AdcionarChar('4')" className="Num">4</button></td>
                    <td><button onclick="AdcionarChar('5')" className="Num">5</button></td>
                    <td><button onclick="AdcionarChar('6')" className="Num">6</button></td>
                    <td><button onclick="Operacao('-')" className="Ope">-</button></td>
                </tr>
                <tr>
                    <td><button onclick="AdcionarChar('1')" className="Num">1</button></td>
                    <td><button onclick="AdcionarChar('2')" className="Num">2</button></td>
                    <td><button onclick="AdcionarChar('3')" className="Num">3</button></td>
                    <td><button onclick="Operacao('+')" className="Ope">+</button></td>
                </tr>
                <tr>
                    <td><button className="Num">+/-</button></td>
                    <td><button onclick="AdcionarChar('0')" className="Num">0</button></td>
                    <td><button onclick="AdcionarChar('.')" className="Num">,</button></td>
                    <td><button onclick="Igual()" className="igual">=</button></td>
                </tr>
            </table>
        </div>
    );
}