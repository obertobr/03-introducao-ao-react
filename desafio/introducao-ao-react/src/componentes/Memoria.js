import React from 'react';

export default function Memoria() {
    return(
        <div className="memoria">
            <table>
                <tr>
                    <td><button onclick="Memoria('C')">MC</button></td>
                    <td><button onclick="Memoria('R')">MR</button></td>
                    <td><button onclick="Memoria('+')">M+</button></td>
                    <td><button onclick="Memoria('-')">M-</button></td>
                    <td><button onclick="Memoria('S')">MS</button></td>
                </tr>
            </table>
        </div>
    );
}